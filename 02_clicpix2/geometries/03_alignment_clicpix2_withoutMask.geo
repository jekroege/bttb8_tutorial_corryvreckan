[CLICpix2_0]
mask_file = "mask_clicpix2_example.conf"
number_of_pixels = 128,128
orientation = -0.0165585deg,0.0410238deg,-1.28738deg
orientation_mode = "xyz"
pixel_pitch = 25um,25um
position = -670.004um,285.517um,106mm
spatial_resolution = 4um,4um
time_resolution = 1ms
role = "reference","dut"
type = "CLICpix2"