[Timepix3_0]
number_of_pixels = 256,256
orientation = 11.0662deg,186.374deg,-1.21358deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 922.323um,285.641um,0
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"

[Timepix3_1]
number_of_pixels = 256,256
orientation = 11.292deg,186.662deg,-0.962569deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -275.557um,397.894um,21.5mm
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"

[Timepix3_2]
number_of_pixels = 256,256
orientation = 10.5845deg,187.075deg,-1.54544deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 5.292um,378.368um,43.5mm
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"
role="reference"

[Timepix3_3]
number_of_pixels = 256,256
orientation = 8.99876deg,8.99412deg,-0.0178763deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -10.202um,-11.943um,186.5mm
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"

[Timepix3_4]
number_of_pixels = 256,256
orientation = 7.79561deg,9.70442deg,1.26538deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 421.587um,-590.797um,208.5mm
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"

[Timepix3_5]
number_of_pixels = 256,256
orientation = 8.01465deg,9.97686deg,0.0731667deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -1.10371mm,-18.72um,231.5mm
spatial_resolution = 4um,4um
time_resolution = 20ns
material_budget = 0.00075
type = "Timepix3"
