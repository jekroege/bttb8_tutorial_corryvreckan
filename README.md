# BTTB8 Corryvreckan Tutorial

**This repo contains the configuration and geomety files for the Corryvreckan hands-on course given at the [BTTB8](https://indico.cern.ch/event/813822/).**

## Which Corryvreckan Version is needed? (Update 2020-08-20)
The examples presented in this tutorial should work out-of-the box.
However, please **do not use the latest master** - a while ago we fixed some coordinate transformation issues which means that "older alignment files" become outdated (or rather would need to be updated by re-doing the alignment procedure).

So for the tutorial, you can **checkout v1.0.2**, which was state-of-the-art at the time of the tutorial such that all things are compatible.

  * **v1.0.2** (and also **v1.0.3**) does not yet include GBL as an option for tracking. This is irrelevant for the examples 1 and 2, which are based on SPS data where straight tracks are perfectly fine. Also, if you hit Ctrl+C, i.e. stop the analysis, too early, you won't get many tracks because we need to wait for the SPS spill in the data. This kicks it at 10sec or so.
  * example 3 also illustrates how to use GBL, for the v1.0.2 is not sufficient. Here you can **checkout 77d9be33e663cae84027310316a785b7f842fc77** (**master branch** at that time). In addition, you need a local EUDAQ2 installation (as described) and enable the EventLoaderEUDAQ2 in cmake.

## For future reference:
All runs are reduced to about 10% of their full size.

__CLICpix2 at SPS__:
* Run 29243 with the alignment file used the [BTTB7 Corryvreckan Tutorial](https://indico.cern.ch/event/731649/contributions/3237289/).

__ATLASpix at SPS__:
* Run 29674 with /testbeam-analysis/cond/Alignment_ATLASpix/aligning_apx_29674_1.geo

__DESY__:
* ATLASpix:
  - Run 715 with /testbeam-analysis/cond/DESY_2019-06/Alignment_June2019_w06s12byHand.geo
* CLIOTD:
  - Run 3390 with clictd/geometry/aligning_201912_run3390_gbl.geo and clictd/geometry/mask_CLICTD_run3390.txt from Magdalena's fork of the testbeam analysis repo
