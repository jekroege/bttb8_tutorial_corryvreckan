[TLU_0]
orientation = 0,0,0
orientation_mode = "xyz"
position = 0,0,0
time_resolution = 1ns
material_budget = 0.0
type = "TLU"
role = "aux"

[MIMOSA26_0]
mask_file = "mask_MIMOSA26_0.txt"
number_of_pixels = 1152,576
orientation = -0.205577deg,0.104164deg,-0.308538deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 233.882um,72.979um,0
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
type = "MIMOSA26"

[MIMOSA26_1]
mask_file = "mask_MIMOSA26_1.txt"
number_of_pixels = 1152,576
orientation = 4.67253deg,0.40829deg,-0.174638deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -452.707um,363.751um,153mm
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
type = "MIMOSA26"

[MIMOSA26_2]
mask_file = "mask_MIMOSA26_2.txt"
number_of_pixels = 1152,576
orientation = -6.62162deg,-0.593355deg,0.0292781deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.111mm,181.497um,305mm
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
type = "MIMOSA26"

[ATLASPix_0]
mask_file = "mask_ATLASpix_w6s12.txt"
number_of_pixels = 25,400
orientation = 180deg,-0,90deg
orientation_mode = "xyz"
pixel_pitch = 130um,40um
position = 77um,10.16um,333mm
spatial_resolution = 40um,10um
time_resolution = 7ns
material_budget = 0.015
role = "dut"
type = "ATLASPix"
time_offset = 0us

[MIMOSA26_3]
mask_file = "mask_MIMOSA26_3.txt"
number_of_pixels = 1152,576
orientation = 6.25968deg,0.630368deg,0.0142094deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.96859mm,414.063um,344mm
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
role = "reference"
type = "MIMOSA26"

[MIMOSA26_4]
mask_file = "mask_MIMOSA26_4.txt"
number_of_pixels = 1152,576
orientation = -7.8984deg,-0.810678deg,-0.355921deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.64456mm,327.574um,456mm
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
type = "MIMOSA26"

[MIMOSA26_5]
mask_file = "mask_MIMOSA26_5.txt"
number_of_pixels = 1152,576
orientation = -8.81014deg,-0.860124deg,0.0635983deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.32324mm,367.305um,576mm
spatial_resolution = 4um,4um
time_resolution = 230us
material_budget = 0.00075
type = "MIMOSA26"

[Timepix3_0]
number_of_pixels = 256,256
orientation = 169.925deg,-0.682622deg,0.605788deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -589.326um,320.869um,666mm
spatial_resolution = 10um,10um
time_resolution = 1.56ns
material_budget = 0.01068
type = "Timepix3"

