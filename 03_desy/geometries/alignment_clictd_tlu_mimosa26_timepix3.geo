[TLU_0]
material_budget = 7.90505e-322
orientation = 0,0,0
orientation_mode = "xyz"
position = 0,0,0
role = "auxiliary"
time_resolution = 1s
type = "tlu"

[MIMOSA26_0]
mask_file = "mask_MIMOSA26_0_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -3.67478deg,-1.41841deg,-0.431953deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 381.47um,221.723um,0
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_1]
mask_file = "mask_MIMOSA26_1_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.569119deg,-0.572786deg,-0.742725deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.14437mm,191.009um,277mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_2]
mask_file = "mask_MIMOSA26_2_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0.155272deg,0.064515deg,0.0013751deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.11946mm,95.476um,305mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[CLICTD_0]
mask_file = "mask_CLICTD_run3390.txt"
material_budget = 0.015
number_of_pixels = 128, 128
orientation = 179.281deg,-0.83033deg,89.2664deg
orientation_mode = "xyz"
pixel_pitch = 37.5um,30um
position = -233.664um,213.936um,344mm
role = "dut"
spatial_resolution = 10.8um,8.7um
time_resolution = 10ns
type = "clictd"

[MIMOSA26_3]
mask_file = "mask_MIMOSA26_3_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.59452deg,0.757393deg,-0.263732deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -667.647um,-20.502um,364mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_4]
mask_file = "mask_MIMOSA26_4_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.89246deg,0.229355deg,-0.0818757deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -500.049um,111.84um,391mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_5]
mask_file = "mask_MIMOSA26_5_thrd6.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -5.42734deg,-1.69034deg,-0.235199deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -305.721um,-445.514um,640mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[Timepix3_0]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 173.512deg,-1.21719deg,-0.235658deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -484.881um,-1.43979mm,685mm
spatial_resolution = 10um,10um
time_offset = 260ns
time_resolution = 20ns
type = "timepix3"

